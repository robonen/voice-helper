<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\FindDictionary;
use App\Entity\PathDictionary;
use App\Service\NormalizeService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/api/path-finder")
 */
class PathFinderConroller extends AbstractController
{
    /**
     * @Route("/", name="get_path", methods={"POST"})
     */
    public function findPath(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent(), true);

        if(is_null($project = $em->getRepository('App:Project')->findOneByCode($data['project'] ?? ''))) {
            return new JsonResponse(array('message' =>'Не существует такого проекта'), JsonResponse::HTTP_BAD_REQUEST);
        }
        $paths = $em->getRepository(FindDictionary::class)->findByProject($project);
        $text = $data['text'] ?? '';

        $max = 0;
        $path = null;
        foreach($paths as $finderPath) {
            $count = 0;
            foreach($finderPath->getWords() as $word) {
                if(strpos(mb_strtolower($text, 'UTF-8'), $word) !== false) $count++;
            }
            if($count >= $max && $count == count($finderPath->getWords())) {
                $path = $finderPath->getPath();
                $max = $count;
            }
        }

        return $this->json([
            'message' => 'Найденный путь',
            'data' => (new NormalizeService())->normalizeByGroup($path),
        ]);
    }

    /**
     * @Route("/project", name="get_paths_for_project", methods={"GET"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function getProjectsPaths(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if(is_null($project = $em->getRepository('App:Project')->findOneByCode($request->query->get('project', '')))) {
            return new JsonResponse(array('message' =>'Не существует такого проекта'), JsonResponse::HTTP_BAD_REQUEST);
        }
        if(!in_array("ROLE_ADMIN", $this->getUser()->getRoles()) && (is_null($project->getManager()) || $project->getManager()->getId() != $this->getUser()->getId())) {
            return new JsonResponse(array('message' =>'Вы не назначены на данный проект'), JsonResponse::HTTP_BAD_REQUEST);
        }
        $paths = $em->getRepository('App:FindDictionary')->findByProject($project);

        return $this->json([
            'message' => 'Пути и словарь слов для данного проекта',
            'data' => (new NormalizeService())->normalizeByGroup($paths),
        ]);
    }

    /**
     * @Route("/all", name="get_all_from_dictionary", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function getAllPaths()
    {
        $em = $this->getDoctrine()->getManager();
        $paths = $em->getRepository('App:FindDictionary')->findAll();

        return $this->json([
            'message' => 'Все пути и словари слов',
            'data' => (new NormalizeService())->normalizeByGroup($paths),
        ]);
    }

    /**
     * @Route("/create", name="add_to_find_dictionary", methods={"POST"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function addPathToDictionary(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if(is_null($path = $em->getRepository('App:PathDictionary')->find($request->request->get('path', null))) || is_null($words = $request->request->get('words', null)) ||
         empty($code = $request->request->get('project', null))) {
            return new JsonResponse(array('message' =>'Не введена вся требуемая информация'), JsonResponse::HTTP_BAD_REQUEST);
        }
        if(is_null($project = $em->getRepository('App:Project')->findOneByCode($code))) {
            return new JsonResponse(array('message' =>'Не существует такого проекта'), JsonResponse::HTTP_BAD_REQUEST);
        }
        if(!in_array("ROLE_ADMIN", $this->getUser()->getRoles()) && (is_null($project->getManager()) || $project->getManager()->getId() != $this->getUser()->getId())) {
            return new JsonResponse(array('message' =>'Вы не назначены на данный проект'), JsonResponse::HTTP_BAD_REQUEST);
        }

        if(is_array($words)) {
            $lc_words = array();
            foreach($words as $word) {
                $word = mb_strtolower($word, 'UTF-8');
                $lc_words[] = $word;
            }
            $words = $lc_words;
        }
        else $words = mb_strtolower($words, 'UTF-8');

        $info = new FindDictionary();
        $info->setPath($path);
        $info->setProject($project);
        $info->setWords(
            is_array($words) ? $words : array($words)
        );

        $em->persist($info);
        $em->flush();

        return $this->json([
            'message' => 'Информация добавлена в словарь поиска',
            'data' => (new NormalizeService())->normalizeByGroup($info),
        ]);
    }

    /**
     * @Route("/update", name="edit_dictionary_info", methods={"POST"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function editDictionaryInfo(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if(is_null($info = $em->getRepository('App:FindDictionary')->find($request->request->get('info', '')))) {
            return new JsonResponse(array('message' =>'В словаре нет такого элемента'), JsonResponse::HTTP_BAD_REQUEST);
        }
        if(!in_array("ROLE_ADMIN", $this->getUser()->getRoles()) && (is_null($info->getProject()->getManager()) || $info->getProject()->getManager()->getId() != $this->getUser()->getId())) {
            return new JsonResponse(array('message' =>'Вы не назначены на данный проект'), JsonResponse::HTTP_BAD_REQUEST);
        }

        $info->setProject(
            is_null($project = $em->getRepository('App:Project')->findOneByCode($request->request->get('project', ''))) ?
            $info->getProject() : $project
        );
        $info->setPath(
            is_null($path = $em->getRepository('App:PathDictionary')->find($request->request->get('path', ''))) ?
            $info->getPath() : $path
        );

        $words = $request->request->get('words', $info->getWords());
        if(is_array($words)) {
            $lc_words = array();
            foreach($words as $word) {
                $word = mb_strtolower($word, 'UTF-8');
                $lc_words[] = $word;
            }
            $words = $lc_words;
        }
        else $words = mb_strtolower($words, 'UTF-8');

        $info->setWords(
            is_array($words) ? $words : array($words)
        );

        $em->flush();

        return $this->json([
            'message' => 'Информация в словаре успешно обновлена',
            'data' => (new NormalizeService())->normalizeByGroup($info),
        ]);
    }

    /**
     * @Route("/words", name="edit_words", methods={"POST"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function editWords(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if(is_null($info = $em->getRepository('App:FindDictionary')->find($request->request->get('info', '')))) {
            return new JsonResponse(array('message' =>'В словаре нет такого элемента'), JsonResponse::HTTP_BAD_REQUEST);
        }
        if(!in_array("ROLE_ADMIN", $this->getUser()->getRoles()) && (is_null($info->getProject()->getManager()) || $info->getProject()->getManager()->getId() != $this->getUser()->getId())) {
            return new JsonResponse(array('message' =>'Вы не назначены на данный проект'), JsonResponse::HTTP_BAD_REQUEST);
        }

        if(!is_null($words = $request->request->get('words', null))) {
            $type = $request->request->get('type', 'add');
            if(is_array($words)) {
                $lc_words = array();
                foreach($words as $word) {
                    $word = mb_strtolower($word, 'UTF-8');
                    $lc_words[] = $word;
                }
                $words = $lc_words;
            }
            else $words = mb_strtolower($words, 'UTF-8');

            if($type == 'add') {
                $array = $info->getWords();
                $array = array_merge($array, is_array($words) ? $words : [$words]);
                $info->setWords(array_unique($array, SORT_REGULAR));
            }
            elseif($type == 'remove') {
                $info->setWords(array_diff($info->getWords(), is_array($words) ? $words : [$words]));
            }
        }

        $em->flush();

        return $this->json([
            'message' => 'Изменен набор слов для данного элемента словаря',
            'data' => (new NormalizeService())->normalizeByGroup($info),
        ]);
    }

    /**
     * @Route("/", name="remove_from_dictionary", methods={"DELETE"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function removeFromFindDictionary(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent(), true);

        if(is_null($info = $em->getRepository('App:FindDictionary')->find($data['info'] ?? null))) {
            return new JsonResponse(array('message' =>'В словаре нет такого элемента'), JsonResponse::HTTP_BAD_REQUEST);
        }
        if(!in_array("ROLE_ADMIN", $this->getUser()->getRoles()) && (is_null($info->getProject()->getManager()) || $info->getProject()->getManager()->getId() != $this->getUser()->getId())) {
            return new JsonResponse(array('message' =>'Вы не назначены на данный проект'), JsonResponse::HTTP_BAD_REQUEST);
        }

        $em->remove($info);
        $em->flush();

        return $this->json([
            'message' => 'Требуемая информаця успешно удалена из словаря',
        ]);
    }
}
