<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Project;
use App\Service\NormalizeService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/api/project")
 */
class ProjectController extends AbstractController
{
    /**
     * @Route("/", name="get_projects", methods={"GET"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function getProjects()
    {
        $em = $this->getDoctrine()->getManager();
        if(in_array("ROLE_ADMIN", $this->getUser()->getRoles()))
            $projects = $em->getRepository('App:Project')->findAll();
        else $projects = $em->getRepository('App:Project')->findByManager($this->getUser());

        return $this->json([
            'message' => 'Список всех проектов',
            'data' => (new NormalizeService())->normalizeByGroup($projects),
        ]);
    }

    /**
     * @Route("/one", name="get_project_by_id", methods={"GET"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function getProject(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $findArray = array();
        if(!in_array("ROLE_ADMIN", $this->getUser()->getRoles())) {
            $findArray['manager'] = $this->getUser();
        }
        if(!is_null($code = $request->query->get('code', null))) {
            $findArray['code'] = $code;
        }
        elseif(!is_null($id = $request->query->get('id', null))) $findArray['id'] = $id;
        else $findArray['id'] = '';

        $project = $em->getRepository('App:Project')->findOneBy($findArray);

        return $this->json([
            'message' => 'Искомый проект',
            'data' => (new NormalizeService())->normalizeByGroup($project),
        ]);
    }

    /**
     * @Route("/", name="add_project", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function addProject(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if(empty($name = $request->request->get('name')) || empty($code = $request->request->get('code'))) {
            return new JsonResponse(array('message' =>'Не указан код или название проекта'), JsonResponse::HTTP_BAD_REQUEST);
        }
        elseif($em->getRepository('App:Project')->findOneByCode($code) instanceof Project) {
            return new JsonResponse(array('message' =>'Проект с таким кодом уже существует в системе'), JsonResponse::HTTP_BAD_REQUEST);
        }
        $project = new Project();
        $project->setName($name);
        $project->setCode($code);

        $em->persist($project);
        $em->flush();

        return $this->json([
            'message' => 'Добавлен новый проект',
            'data' => (new NormalizeService())->normalizeByGroup($project),
        ]);
    }

    /**
     * @Route("/manager", name="set_project_manager", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function setProjectManager(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if(is_null($project = $em->getRepository('App:Project')->findOneByCode($request->request->get('code', '')))) {
            return new JsonResponse(array('message' =>'Не существует такого проекта'), JsonResponse::HTTP_BAD_REQUEST);
        }
        if(is_null($manager = $em->getRepository('App:User')->find($request->request->get('manager', '')))) {
            return new JsonResponse(array('message' =>'Не существует такого пользователя'), JsonResponse::HTTP_BAD_REQUEST);
        }

        if(!is_null($project->getManager()) && $project->getManager() == $manager) 
        $project->setManager(null);
        else $project->setManager($manager);
        $em->flush();

        return $this->json([
            'message' => 'Проекту был назначен менеджер',
            'data' => (new NormalizeService())->normalizeByGroup($project),
        ]);
    }

    /**
     * @Route("/update", name="update_project", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateProject(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if(is_null($project = $em->getRepository('App:Project')->findOneByCode($request->request->get('code', '')))) {
            return new JsonResponse(array('message' =>'Не существует такого проекта'), JsonResponse::HTTP_BAD_REQUEST);
        }
        $project->setName($request->request->get('name', $project->getName()));
        $project->setCode($request->request->get('new_code', $project->getCode()));
        $em->flush();

        return $this->json([
            'message' => 'Проект был обновлен',
            'data' => (new NormalizeService())->normalizeByGroup($project),
        ]);
    }

    /**
     * @Route("/", name="del_project", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function removeProject(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent(), true);

        if(!isset($data['code']) || is_null($project = $em->getRepository('App:Project')->findOneByCode($data['code']))) {
            return new JsonResponse(array('message' =>'Не существует такого проекта'), JsonResponse::HTTP_BAD_REQUEST);
        }

        $em->remove($project);
        $em->flush();

        return $this->json([
            'message' => 'Проект был удален',
        ]);
    }
}
