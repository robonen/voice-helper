<?php

namespace App\Controller;

use App\Entity\PathDictionary;
use App\Service\NormalizeService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/api/path")
 */
class PathDictionaryController extends AbstractController
{
    /**
     * @Route("/", name="get_paths", methods={"GET"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function getPaths()
    {
        $em = $this->getDoctrine()->getManager();
        $paths = $em->getRepository('App:PathDictionary')->findAll();
        return $this->json([
            'message' => 'Все пути из словаря',
            'data' => (new NormalizeService())->normalizeByGroup($paths),
        ]);
    }

    /**
     * @Route("/", name="add_path", methods={"POST"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function addPath(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if(empty($path = $request->request->get('path'))) {
            return new JsonResponse(array('message' =>'Вы не ввели путь'), JsonResponse::HTTP_BAD_REQUEST);
        }
        $newPath = new PathDictionary();
        $newPath->setPath($path);
        $newPath->setParameters(
            is_array($parameters = $request->request->get('parameters', array()))
            ? $parameters : array()
        );
        $newPath->setDescription($request->request->get('description', null));
        $newPath->setNeedAuth(intval($request->request->get('need_auth', 0)) === 1);

        $em->persist($newPath);
        $em->flush();

        return $this->json([
            'message' => 'Новый путь добавлен в словарь',
            'data' => (new NormalizeService())->normalizeByGroup($newPath),
        ]);
    }

    /**
     * @Route("/update", name="update_path", methods={"POST"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function updatePath(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if(is_null($path = $em->getRepository('App:PathDictionary')->find($request->request->get('id', '')))) {
            return new JsonResponse(array('message' =>'Нет такого пути'), JsonResponse::HTTP_BAD_REQUEST);
        }
        $path->setPath($request->request->get('path', $path->getPath()));
        $path->setParameters(
            is_array($parameters = $request->request->get('parameters', ''))
            ? $parameters : $path->getParameters()
        );
        $path->setDescription($request->request->get('description', $path->getDescription()));
        $path->setNeedAuth(intval($request->request->get('need_auth', 0)) === 1);

        $em->flush();

        return $this->json([
            'message' => 'Путь был успешно обновлен',
            'data' => (new NormalizeService())->normalizeByGroup($path),
        ]);
    }

    /**
     * @Route("/parameters", name="update_path_parameters", methods={"POST"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function updatePathParameters(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if(is_null($path = $em->getRepository('App:PathDictionary')->find($request->request->get('id', '')))) {
            return new JsonResponse(array('message' =>'Нет такого пути'), JsonResponse::HTTP_BAD_REQUEST);
        }
        if(!is_null($parameters = $request->request->get('parameters', null))) {
            $type = $request->request->get('type', 'add');
            $currentParams = $path->getParameters();
            if(is_array($parameters)) {
                if($type == 'add') {
                    foreach($parameters as $parameter) {
                        $currentParams[] = $parameter;
                    }
                }
                elseif($type == 'remove') {
                    foreach($parameters as $parameter) {
                        for($i = 0; $i < count($currentParams); $i++) {
                            if($currentParams[$i]['name'] == $parameter['name'] && $currentParams[$i]['value'] == $parameter['value']) {
                                unset($currentParams[$i]);
                            }
                        }
                    }
                }
            }
            $path->setParameters($currentParams);
            $em->flush();
        }

        return $this->json([
            'message' => 'Параметры пути были успешно обновлены',
            'data' => (new NormalizeService())->normalizeByGroup($path),
        ]);
    }

    /**
     * @Route("/", name="del_path", methods={"DELETE"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function removePath(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent(), true);

        if(!isset($data['id']) || is_null($path = $em->getRepository('App:PathDictionary')->find($data['id']))) {
            return new JsonResponse(array('message' =>'Не существует такого пути'), JsonResponse::HTTP_BAD_REQUEST);
        }

        $em->remove($path);
        $em->flush();

        return $this->json([
            'message' => 'Путь был удален',
        ]);
    }
}
