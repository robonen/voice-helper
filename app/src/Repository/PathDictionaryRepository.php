<?php

namespace App\Repository;

use App\Entity\PathDictionary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PathDictionary|null find($id, $lockMode = null, $lockVersion = null)
 * @method PathDictionary|null findOneBy(array $criteria, array $orderBy = null)
 * @method PathDictionary[]    findAll()
 * @method PathDictionary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PathDictionaryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PathDictionary::class);
    }

    // /**
    //  * @return PathDictionary[] Returns an array of PathDictionary objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PathDictionary
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
