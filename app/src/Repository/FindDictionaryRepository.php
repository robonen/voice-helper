<?php

namespace App\Repository;

use App\Entity\FindDictionary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FindDictionary|null find($id, $lockMode = null, $lockVersion = null)
 * @method FindDictionary|null findOneBy(array $criteria, array $orderBy = null)
 * @method FindDictionary[]    findAll()
 * @method FindDictionary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FindDictionaryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FindDictionary::class);
    }

    // /**
    //  * @return FindDictionary[] Returns an array of FindDictionary objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FindDictionary
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
