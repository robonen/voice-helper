<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\PathDictionaryRepository;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PathDictionaryRepository::class)
 */
class PathDictionary
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"main"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"main"})
     */
    private $path;

    /**
     * @ORM\Column(type="array")
     * @Groups({"main"})
     */
    private $parameters = [];

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"main"})
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"main"})
     */
    private $needAuth;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getParameters(): ?array
    {
        return $this->parameters;
    }

    public function setParameters(array $parameters): self
    {
        foreach ($parameters as $key => $value) {
            if ($value === "true") $value = true;
            elseif ($value === "false") $value = false;

            if (is_string($value) && intval($value) && strlen(strval(intval($value))) === strlen($value)) $value = intval($value);

            if (is_string($value) && floatval($value) && strlen(strval(floatval($value))) === strlen($value)) $value = floatval($value);

            $parameters[$key] = $value;
        }
        $this->parameters = $parameters;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNeedAuth(): ?bool
    {
        return $this->needAuth;
    }

    public function setNeedAuth(?bool $needAuth): self
    {
        $this->needAuth = $needAuth;

        return $this;
    }
}
